## Certificados


Los certificados de sello, son un par de archivos CER y KEY, y una contraseña
del archivo KEY. Son generados a partir de la FIEL del emisor. Son usados para
integrar ciertos datos publicos del certificado (CER) y para ***sellar*** los
XML para garantizar su integridad y su ***no repudio***.

Este repositorio incluye una [clase][1] para su validación y manejo completo,
pero veremos como manipularlos con [openssl][2]

Dentro del directorio **cert** de este repositorio, encontraras los certificados
de pruebas de algunos PACs. Usa siempre estos archivos en desarrollo.

Accede a esta carpeta para hacer las siguientes pruebas.

```
cd src/cert
```

## Validaciones necesarias


### Verificar contraseña

El archivo KEY tiene una contraseña establecida al generar los certificados, con
el siguiente comando, la verificamos al convertir el archivo a formato PEM.

```
openssl pkcs8 -inform DER -in finkok.key -passin pass:12345678a -out finkok.pem
```

**CUIDADO**: el archivo resultante, es tu clave privada(KEY) **sin contraseña**,
ten cuidado donde lo dejas.

```
cat finkok.pem

-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCgMwrHopGV5Gsl
YsRJePVxRp/dKmOhLFe6m4uzH+sCb36BHDP2s7gb2oMvShXv/y6iJ3eAOCvycG20
olYiXCdXtDyziYTsG/Jex7nv517Zt2z2Si/fkPOzwBQfNvwsGf6jdUSgAAdRPBsk
NOciJsO9YDe1WyNIvvWJtS0y6LVeb0UiDCRxTcHNXXekOMvNkvwm2Xg0FiGnArf3
OQLJS9KUr/Bel83X3rRm3pd1aIn7AN6jRNavx3j7mvx9TibqchjvYyiXDpFhIeac
InN9RQ1RDMkXhdkwqmCzwB+ZVOduUiaEDqLjhBrrVtp5zpE7fMTpEXffSX0ybzP9
2PWW9A1ZAgMBAAECggEABtFFQyuq8GsRGD00xz+rik+MHBhitwgxD5LxBgV0tdzS
0GHH4FbTnfPVo1yOxHDipinxzvYVALNCJ+7EpZ+wWO4lqypOCtJBpUDwq8H36mIA
9BZCJUyA4oRpFs77OlE84e3qprh5lz5slUTfFTf7GoMOu9wOC1l9D4gzTThUB5m5
ZUDBIYCPoMyzq/bpYP19IZ3EDUcnI6jUPxb7P38cA/9V2x3KbkSez28LjR/Zk/wl
HxOldPHX+zLeep/GGJVCmHNCkdr5lQ1mNNIXBdep/fE1mbdgntJ4vVriFzWafzSG
GHqWvdhC4UfYDXpnlEjjxra7vJ+lfGp7dwJgdm4AgQKBgQDOBZaXexozRYuue12v
n0PJW5DmOmI3yIvJsl4mjpoYiXvNI0zc25Md6y/srrn0mXFQxn1roZ3+2jApH39n
NkX1tnJ0t42M+J3gyo/WJvefWp71juWFLlpoY1f7fm4xzktAv1H7D746k3emyZFw
2GMuEBHPW+ttc+NDnnTrqyfe0QKBgQDHD8dMr41Qiiz7H5+ZY+0oC4ubVXkhU66n
yEaueXH6YM+M1uhmvuI06AG8189Jq5F40oUY8QmKjpF/nAR4oFAxwG8vr6G+694i
zcg73PxlGF9dl2gHzkJs5vnFAMT1QKf1vVS64SyFmxY+rG72d80fjuDGllUl3BsM
mTuEM7e4CQKBgFcMn2uT9ScSzIlF0PkCt1aeHORaaOOfFgl8FzH9cPjLJocv0itD
j67Fj/NWhHXoW6vAon/WwvZK5KyLhX5rvTWsCEJuyB13cuXnUM0wm9I2LRIedbZH
NqYCy8sUyaS7wKoz7ITDSyJe6uCLLKvcklOFMLFH6nrNy4nnqwlkwSzxAoGAXYY9
RftI8qkovwe5jXDUcLcT7XQ4B/9wFKZVBw5KDa/Ec9BYukU+tEq43keqvVsdat3F
6NT+fZnEJGdh9majpuxZkNb8sF3TLRjv1aha8QiFbcx/9QLkVJQkj3lwECZDg4Jj
t7WpnFAkIGgVbjkVJ5mSY9snwDTshHvN6av+yjkCgYEAtZqEtSs0pdKE7Eg7OnRX
+mSreY5qDCKIYjq6STPh35/cn6h2sfw1ts50LLkEqHyKGrDplTMVEsDUYS9/1a5G
HsyiD9K89Sj8tcAFHE0b1MX4eDEfSlJwjzwDu9zGjcD0eZKNZpat2+xCxpbC2qqJ
9Oe7mcqc3W57A1v/zGtRgbw=
-----END PRIVATE KEY-----
```

Si la contraseña es incorrecta, el archivo creado estará vacío y se mostrará el
mensaje de error:

```
Error decrypting key
139754289557504:error:06065064:digital envelope routines:EVP_DecryptFinal_ex:bad decrypt:crypto/evp/evp_enc.c:537:
139754289557504:error:23077074:PKCS12 routines:PKCS12_pbe_crypt:pkcs12 cipherfinal error:crypto/pkcs12/p12_decr.c:63:
139754289557504:error:2306A075:PKCS12 routines:PKCS12_item_decrypt_d2i:pkcs12 pbe crypt error:crypto/pkcs12/p12_decr.c:94:
```

Lo mejor, es poner una contraseña a este archivo.

```
openssl rsa -in finkok.pem -des3 -out finkok.pem.enc

writing RSA key
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
```

Ahora, el certificado debe verse así:


```
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,9933FAA4ED98EA0D

6NU9dpj+2zJEgcdV59WmjqXG8iRNWJAkajQOERIZd1nrANLUzuB5RQ2rgltNmz5O
uF23fg/OaPL/Z9KknJaPc6qtojmnT3pUQSzM21XtkORqeSZuTPwY5vClYqMrAqn/
fGwe7IHDDqUMxA0Q4rjYEZUwX/6EelzjFC6D/7IP7+MTXyN2gXsBGXJDT58rYnzE
JpGnLSvqY6VyxiztHwK1FiPbDh9qPg84v/576If81KSlHXhKDcDU9LsG8bwOieNW
sHYVpOb/YMpnhsO8QPErw/Wwjtk1GzQtDKhFdgG/bjA//0M+QTG/4fMHG0Zggex/
EOvTMJw9laQReMPG72OjULDVCzd03DM+lINlDanjuwh0vDA192gzxx6HWS3rj3cr
VRCwJJBs6QU8Aue0TCOlQut51WJFEdvlECtwGtWJE2wCJi3GQmluLA/0LokRzrWQ
AkRPX2cSx3nfvgtVkCSJg4YvOHOovCBNIwpp6vXaXO+FUh3LcsWV1jGwdjDc156s
+RhfUaF21g250onk7Qfnz0ss8uCKRaEyrvLxj2KJtiZSqOIj+UAQmFPZphxMhXF7
eiFDOMLFQ2wQaERXFfl7GRLNfeEaffdJzulCfjKgQYnYlg9Hwebl27xnhJBUUBL2
sGGvA221UF+mi+KKCriW+e6LdElSthdlM8PkxywJDAI4dTdCnqSs6E1QjC4SsbAr
stnCcU0ptki1zd+7APijSpEYJlbASUlgxUwnUFn4oa4Mf/mwbTEJcg6QNRfszcjC
khPvMAPuV/0NsyjDMkRKK0c4CpYHqvwp1yMuTLldpRnIBPptueatcsvGClsytcxj
rFIgw9mcoFVfCqR/KtrAq0gny+UgV0GRBGgDwuaUBrYNuxcpw8v3UbF3PkQFW+8s
brbJQTXSOyA78/T5sH/iJCkUseh7xKfi0C4SChQmv+vz80Fcd4W8uWd27o38rncw
q0XhHbqOPSR5P54U0G8NRMm+5FEWDZVRzcjVHS5jtR+k71aJ66HHzfjMDF4J37xy
6oOOWNdNfNfvugOKalKkDETjfb8b4NfMp0k5b8auAVULnqhy6ZdOro7rdCZmS4Un
fvg0+IhAuvgenUl9wBsVAUySRBLvTG7G3dF71ojW6aCJFWmZ3PqNvo2uwOi2nJEv
aNya9MUDc4zh+EbzfBXNLt7cztaRcFJ0STziGVuuI/zQwjdAanRrZjuiuM62lfu2
hRmnEWjgpJXLkSgCHGxWQIU7Zru+480bqxTnaVwbej7ZD4cS4YPhTWLEw0mDwARF
TNGQyInamywU2/6nZuUG7xiYlFbZ7UvBXtz0xnk+vNK8BOWTg7N+/ntCn5ZC435+
Pi3HszuZfuC5ewxy2105fSl7/2JzDSFJciJPPVZjgMRbB2mZXXH7h9X3j6esRhF9
sIhq4yYpyYvvOxzfeMgaXc1QnDKdQAyOB7hUoM20avH7RKVzMVnidkUI6m4CbqdQ
IyZ4pHoa++teRJVBeg6TNh0BZJtBn1X8BXhm9B977lB7iIDEUaRtIFJR/3kEKM5K
71/4KFCnYYBjm1ARFR0/vD0R2ZGHuV8IpvIalShjs1PxmYsW9P6U3g==
-----END RSA PRIVATE KEY-----
```

Ya puedes borrar el primer archivo PEM generado.

```
rm finkok.pem
```


### Que no sea FIEL

Los certificados de sello, tecnicamente son iguales a la FIEL, pero son
derivados de esta, por lo que hay que asegurarse que **no sean FIEL**.

```
openssl x509 -inform DER -in finkok.cer -noout -purpose

Certificate purposes:
SSL client : Yes
SSL client CA : No
SSL server : Yes
SSL server CA : No
Netscape SSL server : No
Netscape SSL server CA : No
S/MIME signing : Yes
S/MIME signing CA : No
S/MIME encryption : No
S/MIME encryption CA : No
CRL signing : No
CRL signing CA : No
Any Purpose : Yes
Any Purpose CA : Yes
OCSP helper : Yes
OCSP helper CA : No
Time Stamp signing : No
Time Stamp signing CA : No
```

La cuarta línea de esta salida es: `SSL server : Yes`, que nos indica que es
un certificado de sellos, si fuera una **FIEL**, la salida sería:
`SSL server : No`


```
openssl x509 -inform DER -in finkok.cer -noout -purpose | sed '4!d'

SSL server : Yes
```


### Que sean pareja

Los certificados de sello, siempre se corresponden en pareja. Obteniendo el
**modulo** de cada archivo, se puede verificar esto.

```
openssl x509 -inform DER -in finkok.cer -noout -modulus

Modulus=A0330AC7A29195E46B2562C44978F571469FDD2A63A12C57BA9B8BB31FEB026F7E811C33F6B3B81BDA832F4A15EFFF2EA2277780382BF2706DB4A256225C2757B43CB38984EC1BF25EC7B9EFE75ED9B76CF64A2FDF90F3B3C0141F36FC2C19FEA37544A00007513C1B2434E72226C3BD6037B55B2348BEF589B52D32E8B55E6F45220C24714DC1CD5D77A438CBCD92FC26D978341621A702B7F73902C94BD294AFF05E97CDD7DEB466DE97756889FB00DEA344D6AFC778FB9AFC7D4E26EA7218EF6328970E916121E69C22737D450D510CC91785D930AA60B3C01F9954E76E5226840EA2E3841AEB56DA79CE913B7CC4E91177DF497D326F33FDD8F596F40D59

openssl rsa -in finkok.pem.enc -noout -modulus

Enter pass phrase for finkok.pem.enc:

Modulus=A0330AC7A29195E46B2562C44978F571469FDD2A63A12C57BA9B8BB31FEB026F7E811C33F6B3B81BDA832F4A15EFFF2EA2277780382BF2706DB4A256225C2757B43CB38984EC1BF25EC7B9EFE75ED9B76CF64A2FDF90F3B3C0141F36FC2C19FEA37544A00007513C1B2434E72226C3BD6037B55B2348BEF589B52D32E8B55E6F45220C24714DC1CD5D77A438CBCD92FC26D978341621A702B7F73902C94BD294AFF05E97CDD7DEB466DE97756889FB00DEA344D6AFC778FB9AFC7D4E26EA7218EF6328970E916121E69C22737D450D510CC91785D930AA60B3C01F9954E76E5226840EA2E3841AEB56DA79CE913B7CC4E91177DF497D326F33FDD8F596F40D59
```

**Los dos modulos deben ser iguales**


### Convertir el CER en texto plano

Esto es necesario para agregarlo como un atributo de CFDI, se usa para validar
el sello del mismo.

```
openssl enc -base64 -in finkok.cer -out finkok.txt

cat finkok.txt

MIIF8DCCA9igAwIBAgIUMjAwMDEwMDAwMDAzMDAwMjI3NjIwDQYJKoZIhvcNAQEL
BQAwggFmMSAwHgYDVQQDDBdBLkMuIDIgZGUgcHJ1ZWJhcyg0MDk2KTEvMC0GA1UE
CgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNV
BAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNp
w7NuMSkwJwYJKoZIhvcNAQkBFhphc2lzbmV0QHBydWViYXMuc2F0LmdvYi5teDEm
MCQGA1UECQwdQXYuIEhpZGFsZ28gNzcsIENvbC4gR3VlcnJlcm8xDjAMBgNVBBEM
BTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQRGlzdHJpdG8gRmVkZXJhbDES
MBAGA1UEBwwJQ295b2Fjw6FuMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxITAfBgkq
hkiG9w0BCQIMElJlc3BvbnNhYmxlOiBBQ0RNQTAeFw0xNjEwMjEyMDQ3NDVaFw0y
MDEwMjEyMDQ3NDVaMIHcMSgwJgYDVQQDEx9FSklETyBST0RSSUdVRVogUFVFQkxB
IFNBIERFIENWMSgwJgYDVQQpEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERF
IENWMSgwJgYDVQQKEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSUw
IwYDVQQtExxUQ005NzA2MjVNQjEgLyBIRUdUNzYxMDAzNFMyMR4wHAYDVQQFExUg
LyBIRUdUNzYxMDAzTURGUk5OMDkxFTATBgNVBAsUDFBydWViYXNfQ0ZESTCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKAzCseikZXkayVixEl49XFGn90q
Y6EsV7qbi7Mf6wJvfoEcM/azuBvagy9KFe//LqInd4A4K/JwbbSiViJcJ1e0PLOJ
hOwb8l7Hue/nXtm3bPZKL9+Q87PAFB82/CwZ/qN1RKAAB1E8GyQ05yImw71gN7Vb
I0i+9Ym1LTLotV5vRSIMJHFNwc1dd6Q4y82S/CbZeDQWIacCt/c5AslL0pSv8F6X
zdfetGbel3VoifsA3qNE1q/HePua/H1OJupyGO9jKJcOkWEh5pwic31FDVEMyReF
2TCqYLPAH5lU525SJoQOouOEGutW2nnOkTt8xOkRd99JfTJvM/3Y9Zb0DVkCAwEA
AaMdMBswDAYDVR0TAQH/BAIwADALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQELBQAD
ggIBAA+okCrsYf2Pl6phFwLFuoNvO4zcGPCQsRrl89ZbDDgdThL3iAoi0wbDOl9+
EcJiJTEfDdo8sa6c3Y5ubfZ8zog3SdlguL+Fb5Bz7B1sj2hdQFDtvZl5gkE3tdif
4OSMhLQIolBsv4746DM7dtOTKcj3HiwO6KbBPqIFxf6B/zy74Gafg4r6DoiSnp12
vTh53fDKOjKB7EIX9+MbuWfwnqtg0ZMvknOpYkLCfDJTIXDNhgk6ykwvaaPxilMM
dvJSRutWBprKEZS5G26wSLnnIhW6J8Xm79z8nwQYrGt6TfbjCvFN7KbFaV1c6hLv
5cXil2kdirf0CpZWvDEI2ZfQKj2UP0As7z7eIl7VnY8lbIg/JNApOimZ+fLgmikH
sSfqE94YzjTB3LLIYsacLA8pOWqm/twkUkCFIC7x+WZIyCtlyegzQdv1I+95Qs5/
3RKb9J65LPlvMJgPHVPRGSIObDLiskqGINNmaULB3pABqxP9XkSzpPQI4ME9Jacz
TN9/mAEoypr7DBRP2ZpeJMusIVvc88Ih2LhBeonza7MiP8uBRVMLSfGUu+Antdgk
3Az5q/3Qz+4CvEex9vNL24bMXSfM7mK+Yalw6LeKvDW4SMt+JHQ5fp3cBVyUbWgl
mjjSt2ehYDjR2t+eIuxqyyshy7iJ2QleM0fuHE0L2GB3C8Rw
```

Dentro del archivo hay que eliminar los saltos de línea.

```
openssl enc -base64 -in finkok.cer | tr -d "\n" > finkok.txt
```

### Serie del certificado

Este valor también debe ir como atributo dentro del CFDI.

```
openssl x509 -inform DER -in finkok.cer -noout -serial

serial=3230303031303030303030333030303232373632
```

Hay que tomar los dígitos pares de esta cadena para que quede:

```
openssl x509 -inform DER -in finkok.cer -noout -serial | cut -c8- | sed -r 's/.(.)/\1/g'

20001000000300022762
```

### Obtener las fechas de vigencia

Cuando se sella un CFDI, el mismo debe hacerse con un certificado **vigente**.

```
openssl x509 -inform DER -in finkok.cer -noout -dates

notBefore=Oct 21 20:47:45 2016 GMT
notAfter=Oct 21 20:47:45 2020 GMT
```

### Obtener el propietario (Razón Social) y el RFC

El RFC debe corresponder con el RFC del emisor del CFDI.

```
openssl x509 -inform DER -in finkok.cer -noout -subject

subject=CN = EJIDO RODRIGUEZ PUEBLA SA DE CV, name = EJIDO RODRIGUEZ PUEBLA SA DE CV, O = EJIDO RODRIGUEZ PUEBLA SA DE CV, x500UniqueIdentifier = TCM970625MB1 / HEGT7610034S2, serialNumber = " / HEGT761003MDFRNN09", OU = Pruebas_CFDI
```

Es buena practica validar todos estos datos.

Esto es todo lo que se requiere en cuanto a certificados.

Este repositorio cuenta con una herramienta para trabajar con los certificados.
Te ayuda a validarlos y verificar que son correctos. Si ejecutas.

```
python text2cfdi.py -vc

Introduce la contraseña del certificado:

Validando certificados...

    RFC: TCM970625MB1
    No de Serie: 20001000000300022762
    Válido desde: 2016-10-21 20:47:45
    Válido hasta: 2020-10-21 20:47:45
    Es vigente: True
    Son pareja: True
    Es FIEL: False

Los certificados son válidos
```

Dado que de forma predeterminada el script se ejecuta en modo **depuración**, de
forma automática se valida el certificado de pruebas del PAC.

Más adelante te mostraremos como validar certificados en producción.


[1]: https://gitlab.com/mauriciobaeza/text2cfdi/blob/master/src/util.py#L92
[2]: https://www.openssl.org/
