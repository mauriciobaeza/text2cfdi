## Generar el XML


El XML es un simple archivo de texto, con una estructura estándar y bien definida.
Esta estructura la define claramente el socio de todos los mexicanos el SAT en
la página de documentación de la [Factura Electrónica][1]

La versión vigente de CFDI es la 3.3 vigente desde el 01 de Julio del 2017

El siguiente es un XML mínimo de un CFDI.

```
<?xml version='1.0' encoding='utf-8'?>
<cfdi:Comprobante
    xmlns:cfdi="http://www.sat.gob.mx/cfd/3"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    TipoCambio="1"
    Moneda="MXN"
    TipoDeComprobante="I"
    LugarExpedicion="06850"
    SubTotal="1000.00"
    Total="1160.00"
    Version="3.3"
    Fecha="2019-02-08T22:01:09"
    xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd">
  <cfdi:Emisor Rfc="TCM970625MB1" RegimenFiscal="601"/>
  <cfdi:Receptor Rfc="BASM740115RW0" UsoCFDI="G01"/>
  <cfdi:Conceptos>
    <cfdi:Concepto ClaveProdServ="60121001" Cantidad="1.0" ClaveUnidad="KGM" Descripcion="Asesoría en desarrollo" ValorUnitario="1000.00" Importe="1000.00">
      <cfdi:Impuestos>
        <cfdi:Traslados>
          <cfdi:Traslado Base="1000.00" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="160.00"/>
        </cfdi:Traslados>
      </cfdi:Impuestos>
    </cfdi:Concepto>
  </cfdi:Conceptos>
  <cfdi:Impuestos TotalImpuestosTrasladados="160.00">
    <cfdi:Traslados>
      <cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="160.00"/>
    </cfdi:Traslados>
  </cfdi:Impuestos>
</cfdi:Comprobante>
```

Con esta herramienta, puedes convertir un archivo en formato JSON en CFDI, dentro
de la carpeta **ejemplos** tienes varios para probar.

El archivo XML mostrado más arriba, se generó desde el siguiente archivo:

```
{
    "comprobante" : {
        "NoCertificado": "20001000000300022815",
        "TipoCambio": "1",
        "Moneda": "MXN",
        "TipoDeComprobante": "I",
        "LugarExpedicion": "06850",
        "SubTotal": "1000.00",
        "Total": "1160.00"
    },
    "emisor": {
        "Rfc": "TCM970625MB1",
        "RegimenFiscal": "601"
    },
    "receptor": {
        "Rfc": "BASM740115RW0",
        "UsoCFDI": "G01"
    },
    "conceptos": [
        {
            "ClaveProdServ": "60121001",
            "Cantidad": "1.0",
            "ClaveUnidad": "KGM",
            "Descripcion": "Asesoría en desarrollo",
            "ValorUnitario": "1000.00",
            "Importe": "1000.00",
            "impuestos": {
                "traslados": [
                    {
                        "Base": "1000.00",
                        "Impuesto": "002",
                        "TipoFactor": "Tasa",
                        "TasaOCuota": "0.160000",
                        "Importe": "160.00"
                    }
                ]
            }
        }
    ],
    "impuestos": {
        "TotalImpuestosTrasladados": "160.00",
        "traslados": [
            {
                "Impuesto": "002",
                "TipoFactor": "Tasa",
                "TasaOCuota": "0.160000",
                "Importe": "160.00"
            }
        ]
    }
}
```

Con la instrucción:

```
python text2cfdi.py -d ~/pruebas -g

Generando el documento 1 de 1: /home/mau/pruebas/entrada/cfdi_minimo.json
    Documento generado: /home/mau/pruebas/salida/cfdi_minimo.xml
```

La mayor parte de los lenguajes modernos, tienen alguna librería para generar
XML validos.


[1]: http://omawww.sat.gob.mx/informacion_fiscal/factura_electronica/Paginas/default.aspx
