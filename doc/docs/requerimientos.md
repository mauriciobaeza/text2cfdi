## Requerimientos previos


Para usar este script requerimos:

* Python 2 o 3 (se recomienda Py3, Py2 debe morir ya)
* lxml
* cryptography
* click
* logbook
* pyyaml
* zeep

Para aprender como se hace a mano todo el proceso, también vamos a necesitar.

* [openssl][1]
* xsltproc

Asegurate de que esten instalados y accesibles en tu sistema.

```
openssl version
OpenSSL 1.1.1a  20 Nov 2018

xsltproc -V
Using libxml 20909, libxslt 10133-GITv1.1.33 and libexslt 820
xsltproc was compiled against libxml 20908, libxslt 10133 and libexslt 820
libxslt 10133 was compiled against libxml 20908
libexslt 820 was compiled against libxml 20908
```

También son necesarios un par de certificados de sello, este repositorio ya
incluye los de pruebas de varios PACs

Lo mejor es usar un entorno virtual

```
python -m venv cfdi
```

Accede y activa el entorno

```
cd cfdi
source bin/activate
```

Ahora clona el repositorio:

```
git clone https://gitlab.com/mauriciobaeza/text2cfdi.git

```

Accede al directorio

```
cd text2cfdi
```

Instala los requerimientos

```
pip install -r requirements.txt
...
Successfully installed appdirs-1.4.3 asn1crypto-0.24.0 attrs-18.2.0 cached-property-1.5.1 certifi-2018.11.29 cffi-1.11.5 chardet-3.0.4 click-7.0 cryptography-2.5 defusedxml-0.5.0 idna-2.8 isodate-0.6.0 logbook-1.4.3 lxml-4.3.0 pycparser-2.19 pytz-2018.9 pyyaml-3.13 requests-2.21.0 requests-toolbelt-0.9.1 six-1.12.0 urllib3-1.24.1 zeep-3.2.0

```

Crea una carpeta de trabajo donde quieras.

```
mkdir ~/pruebas
mkdir ~/pruebas/entrada
mkdir ~/pruebas/salida
mkdir ~/pruebas/timbrados
```

Copia el archivo

```
cp ejemplos/cfdi_minimo.json ~/pruebas/entrada/
```

Accede al directorio

```
cd src
```

Renombras el archivo:

```
mv conf.py.example conf.py
```

Y ejecutas

```
python text2cfdi.py -vc

Introduce la contraseña del certificado:
```

Captura la contraseña: `12345678a`

Debes de ver:

```
Validando certificados...

    RFC: TCM970625MB1
    No de Serie: 20001000000300022762
    Válido desde: 2016-10-21 20:47:45
    Válido hasta: 2020-10-21 20:47:45
    Es vigente: True
    Son pareja: True
    Es FIEL: False

Los certificados son válidos
```

Ahora, ejecutas:

```
python text2cfdi.py -d ~/pruebas -gst

Generando el documento 1 de 1: /home/mau/pruebas/entrada/cfdi_minimo.json
    Documento generado: /home/mau/pruebas/salida/cfdi_minimo.xml
Sellando el documento 1 de 1: /home/mau/pruebas/salida/cfdi_minimo.xml
    Documento sellado...
Enviando a timbrar el documento 1 de 1: /home/mau/pruebas/salida/cfdi_minimo.xml
    Documento timbrado: /home/mau/pruebas/timbrados/cfdi_minimo.xml
```

Si llegaste hasta aquí sin ningún error, felicidades, si no, usa el
[sistema de tickets][2] del proyecto para ayudarte a resolverlo.

En los siguientes temas, veremos paso a paso como se hace el proceso.


[1]: https://www.openssl.org/
[2]: https://gitlab.com/mauriciobaeza/text2cfdi/issues
