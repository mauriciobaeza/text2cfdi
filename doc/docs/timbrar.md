## Timbrar el XML

El proceso de timbrado, es exactamente el mismo visto en el sellado, la única
diferencia, es que solo un Proveedor Autorizado de Certificación (PAC de aquí
en adelante), autorizado por SAT obviamente, puede hacer este timbrado. Es decir
este PAC, le agrega una segunda firma digital a nuestro CFDI

Cada PAC implementa su propio estandar para esto, aunque la mayoría usa webservice.

Para enviar a timbrar el documento que previamente hemos sellado, usamos.

```
python text2cfdi.py -d ~/pruebas/ -t

Enviando a timbrar el documento 1 de 1: /home/mau/pruebas/salida/cfdi_minimo.xml
    Documento timbrado: /home/mau/pruebas/timbrados/cfdi_minimo.xml
```

El cual será guardado en la subcarpeta ***timbrados*** de nuestra carpeta de
trabajo. Si vemos el documento.

```
<?xml version='1.0' encoding='utf-8'?>
<cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" NoCertificado="20001000000300022762" TipoCambio="1" Moneda="MXN" TipoDeComprobante="I" LugarExpedicion="06850" SubTotal="1000.00" Total="1160.00" Version="3.3" Fecha="2019-02-08T22:38:45" Certificado="MIIF8DCCA9igAwIBAgIUMjAwMDEwMDAwMDAzMDAwMjI3NjIwDQYJKoZIhvcNAQELBQAwggFmMSAwHgYDVQQDDBdBLkMuIDIgZGUgcHJ1ZWJhcyg0MDk2KTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSkwJwYJKoZIhvcNAQkBFhphc2lzbmV0QHBydWViYXMuc2F0LmdvYi5teDEmMCQGA1UECQwdQXYuIEhpZGFsZ28gNzcsIENvbC4gR3VlcnJlcm8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQRGlzdHJpdG8gRmVkZXJhbDESMBAGA1UEBwwJQ295b2Fjw6FuMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxITAfBgkqhkiG9w0BCQIMElJlc3BvbnNhYmxlOiBBQ0RNQTAeFw0xNjEwMjEyMDQ3NDVaFw0yMDEwMjEyMDQ3NDVaMIHcMSgwJgYDVQQDEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSgwJgYDVQQpEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSgwJgYDVQQKEx9FSklETyBST0RSSUdVRVogUFVFQkxBIFNBIERFIENWMSUwIwYDVQQtExxUQ005NzA2MjVNQjEgLyBIRUdUNzYxMDAzNFMyMR4wHAYDVQQFExUgLyBIRUdUNzYxMDAzTURGUk5OMDkxFTATBgNVBAsUDFBydWViYXNfQ0ZESTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKAzCseikZXkayVixEl49XFGn90qY6EsV7qbi7Mf6wJvfoEcM/azuBvagy9KFe//LqInd4A4K/JwbbSiViJcJ1e0PLOJhOwb8l7Hue/nXtm3bPZKL9+Q87PAFB82/CwZ/qN1RKAAB1E8GyQ05yImw71gN7VbI0i+9Ym1LTLotV5vRSIMJHFNwc1dd6Q4y82S/CbZeDQWIacCt/c5AslL0pSv8F6XzdfetGbel3VoifsA3qNE1q/HePua/H1OJupyGO9jKJcOkWEh5pwic31FDVEMyReF2TCqYLPAH5lU525SJoQOouOEGutW2nnOkTt8xOkRd99JfTJvM/3Y9Zb0DVkCAwEAAaMdMBswDAYDVR0TAQH/BAIwADALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQELBQADggIBAA+okCrsYf2Pl6phFwLFuoNvO4zcGPCQsRrl89ZbDDgdThL3iAoi0wbDOl9+EcJiJTEfDdo8sa6c3Y5ubfZ8zog3SdlguL+Fb5Bz7B1sj2hdQFDtvZl5gkE3tdif4OSMhLQIolBsv4746DM7dtOTKcj3HiwO6KbBPqIFxf6B/zy74Gafg4r6DoiSnp12vTh53fDKOjKB7EIX9+MbuWfwnqtg0ZMvknOpYkLCfDJTIXDNhgk6ykwvaaPxilMMdvJSRutWBprKEZS5G26wSLnnIhW6J8Xm79z8nwQYrGt6TfbjCvFN7KbFaV1c6hLv5cXil2kdirf0CpZWvDEI2ZfQKj2UP0As7z7eIl7VnY8lbIg/JNApOimZ+fLgmikHsSfqE94YzjTB3LLIYsacLA8pOWqm/twkUkCFIC7x+WZIyCtlyegzQdv1I+95Qs5/3RKb9J65LPlvMJgPHVPRGSIObDLiskqGINNmaULB3pABqxP9XkSzpPQI4ME9JaczTN9/mAEoypr7DBRP2ZpeJMusIVvc88Ih2LhBeonza7MiP8uBRVMLSfGUu+Antdgk3Az5q/3Qz+4CvEex9vNL24bMXSfM7mK+Yalw6LeKvDW4SMt+JHQ5fp3cBVyUbWglmjjSt2ehYDjR2t+eIuxqyyshy7iJ2QleM0fuHE0L2GB3C8Rw" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" Sello="K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg==">
  <cfdi:Emisor Rfc="TCM970625MB1" RegimenFiscal="601"/>
  <cfdi:Receptor Rfc="BASM740115RW0" UsoCFDI="G01"/>
  <cfdi:Conceptos>
    <cfdi:Concepto ClaveProdServ="60121001" Cantidad="1.0" ClaveUnidad="KGM" Descripcion="Asesoría en desarrollo" ValorUnitario="1000.00" Importe="1000.00">
      <cfdi:Impuestos>
        <cfdi:Traslados>
          <cfdi:Traslado Base="1000.00" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="160.00"/>
        </cfdi:Traslados>
      </cfdi:Impuestos>
    </cfdi:Concepto>
  </cfdi:Conceptos>
  <cfdi:Impuestos TotalImpuestosTrasladados="160.00">
    <cfdi:Traslados>
      <cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="160.00"/>
    </cfdi:Traslados>
  </cfdi:Impuestos>
  <cfdi:Complemento>
    <tfd:TimbreFiscalDigital xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" Version="1.1" SelloCFD="K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg==" NoCertificadoSAT="20001000000300022323" RfcProvCertif="FIN1203015JA" UUID="01BFFB48-A79A-4173-AE0B-F46C6971D0D7" FechaTimbrado="2019-02-08T22:50:15" SelloSAT="moqnylhqsNDe/uJigxtxFMTCEbgRDrnshybg+rObm3ljThkWpiMLguGJiRepvg4J8BfllQUMyJ97EoAAYzLddpO9E46Dpz2SAXq7O0A6++Yi2QtIcIuCqCbNBSo8xM/IrchGUSkvm52fZt5OgCEcYwd82jIsrst8sBNmUP8UHgHXrh1Lq8PsNPK2TGJ5Yw051eh3MOYakHp+02FYYpFK7jf2WUvifj978xP5raSZhL57XxWrsnGtlmoFz7D+8CuG48+3yD8ic9Sm0XhZQpiSa+zYmfUjrQ8ldEStMlaVG5sn5ljeTG5eEay7yc4TVt5RlLmyn7eoHea3IjJkK+I5Tw=="/>
  </cfdi:Complemento>
</cfdi:Comprobante>
```

El PAC, agrega un nuevo nodo con el timbre fiscal y otros datos importantes.

```
  <cfdi:Complemento>
    <tfd:TimbreFiscalDigital
        xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"
        xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd"
        Version="1.1"
        SelloCFD="K6ZPOaZfgy5DK8iOdhGOn6hBH5LtEazLfWLZExD23Ro31oYgta+V8CTrx63mL1Tozi//M6ZATY8P9TUXdFFINAKasSRmLA+89YYvasmXu46BQ01IQnf4j00H3Dx1FrushSegMYmszXkWvvLX0F3wH0cDz7aGPXs5cZ4Oy/F7Sn86REigE22gKrgvNpD1X63rAZD1q3BNgJ83Ol1OSa8ok59xvidu4A/8GvLMM511gtmDPtFpC6H1Nn38rRFCKpvazUYy+d/YNf14SdUQIC0uFtYu2gwGdxxJZwEo/2HXzcJxgGa5Wg2yrRLIX//kxz0XGrhvP9c0IQKQ874nSdbVHg=="
        NoCertificadoSAT="20001000000300022323"
        RfcProvCertif="FIN1203015JA"
        UUID="01BFFB48-A79A-4173-AE0B-F46C6971D0D7"
        FechaTimbrado="2019-02-08T22:50:15"
        SelloSAT="moqnylhqsNDe/uJigxtxFMTCEbgRDrnshybg+rObm3ljThkWpiMLguGJiRepvg4J8BfllQUMyJ97EoAAYzLddpO9E46Dpz2SAXq7O0A6++Yi2QtIcIuCqCbNBSo8xM/IrchGUSkvm52fZt5OgCEcYwd82jIsrst8sBNmUP8UHgHXrh1Lq8PsNPK2TGJ5Yw051eh3MOYakHp+02FYYpFK7jf2WUvifj978xP5raSZhL57XxWrsnGtlmoFz7D+8CuG48+3yD8ic9Sm0XhZQpiSa+zYmfUjrQ8ldEStMlaVG5sn5ljeTG5eEay7yc4TVt5RlLmyn7eoHea3IjJkK+I5Tw=="/>
  </cfdi:Complemento>

```

Felicidades, tenemos un documento CFDI sellado y timbrado correctamente.

Si eres PAC y estas leyendo esto, estaremos encantados de agregar un ejemplo de
integración de tu servicio a este proyecto.

