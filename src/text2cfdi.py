#!/usr/bin/env python

import click
import util
from settings import log, DEBUG, PATH_CERT, FOLDERS


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-d', '--dir-trabajo', default='')
@click.option('-de', '--dir-entrada', default='')
@click.option('-ds', '--dir-salida', default='')
@click.option('-dt', '--dir-timbrados', default='')
@click.option('-dc', '--dir-certificados', default='')
@click.option('-vc', '--validar-certificados', is_flag=True, default=False)
@click.option('-g', '--generar', is_flag=True, default=False)
@click.option('-s', '--sellar', is_flag=True, default=False)
@click.option('-t', '--timbrar', is_flag=True, default=False)
@click.option('-c', '--cancelar', is_flag=True, default=False)
@click.option('-e', '--estatus-cfdi', is_flag=True, default=False)
@click.option('-u', '--uuid', default='')
def main(
    dir_trabajo,
    dir_entrada,
    dir_salida,
    dir_timbrados,
    dir_certificados,
    validar_certificados,
    generar,
    sellar,
    timbrar,
    cancelar,
    estatus_cfdi,
    uuid):

    if DEBUG:
        dir_certificados = PATH_CERT

    if validar_certificados:
        util.validate_cert(dir_certificados)
        return

    if cancelar:
        util.cancelar_cfdi(uuid, dir_certificados)
        return

    dir_entrada = dir_entrada or util.join(dir_trabajo, FOLDERS['ENTRADA'])
    dir_salida = dir_salida or util.join(dir_trabajo, FOLDERS['SALIDA'])
    dir_timbrados = dir_timbrados or util.join(dir_trabajo, FOLDERS['TIMBRADOS'])

    if not util.validar_dir(dir_entrada, dir_salida, dir_timbrados):
        return

    if generar:
        util.generar_cfdi(dir_entrada, dir_salida, dir_certificados)
    if sellar:
        util.sellar_cfdi(dir_salida, dir_certificados)
    if timbrar:
        util.timbrar_cfdi(dir_salida, dir_timbrados)
    if estatus_cfdi:
        util.estatus_cfdi(uuid, dir_timbrados)

    return


if __name__ == '__main__':
    main()
