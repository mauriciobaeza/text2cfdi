#!/usr/bin/env python
# coding: utf-8

import base64
import codecs
import datetime
import getpass
import json
import os
import re
import sys
from collections import OrderedDict

import click
import yaml
import lxml.etree as ET
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.x509.oid import ExtensionOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

from settings import DEBUG, TOKEN, PY2, CERT_NAME, DELETE_SOURCE, PATH_XSLT, PAC


def join(*args):
    return os.path.join(*args)


def _msg(msg, bold=True, fg=''):
    click.secho(msg, fg=fg, bold=bold)
    return


def _error(msg):
    click.secho(msg, fg='red', bold=True)
    return


def _get_files(path, ext='yaml|json|txt'):
    docs = []
    for folder, _, files in os.walk(path):
        pattern = re.compile('(.*)\.({})'.format(ext), re.IGNORECASE)
        docs += [join(folder, f) for f in files if pattern.search(f)]
    return tuple(docs)


def _change_ext(path, ext='xml'):
    path, filename = os.path.split(path)
    name, extension = os.path.splitext(filename)
    return '{}.{}'.format(name, ext)


def _get_file_name(path):
    _, filename = os.path.split(path)
    return filename


def _exists(path):
    return os.path.exists(path)


def _read_file(path):
    with open(path, 'rb') as f:
        data = f.read()
    return data


def _save_file(path, data):
    if PY2:
        with codecs.open(path, 'w', encoding='utf-8') as f:
            f.write(data)
    else:
        with open(path, 'w', encoding='utf-8') as f:
            if isinstance(data, str):
                f.write(data)
            else:
                f.write(data.decode('utf-8'))
    return

def _delete_source(path):
    if DELETE_SOURCE and not DEBUG:
        try:
            os.remove(path)
        except:
            pass
    return


def _add_stamp(path_xml, path_xlst, cert):
    return True


class SATCertificate(object):

    def __init__(self, cer=b'', key=b'', password=''):
        self._error = ''
        self._init_values()
        self._get_data_cer(cer)
        self._get_data_key(key, password)

    def _init_values(self):
        self._rfc = ''
        self._serial_number = ''
        self._not_before = None
        self._not_after = None
        self._is_fiel = False
        self._are_couple = False
        self._is_valid_time = False
        self._cer_pem = ''
        self._cer_txt = ''
        self._key_enc = b''
        self._cer_modulus = 0
        self._key_modulus = 0
        return

    def __str__(self):
        msg = '\tRFC: {}\n'.format(self.rfc)
        msg += '\tNo de Serie: {}\n'.format(self.serial_number)
        msg += '\tVálido desde: {}\n'.format(self.not_before)
        msg += '\tVálido hasta: {}\n'.format(self.not_after)
        msg += '\tEs vigente: {}\n'.format(self.is_valid_time)
        msg += '\tSon pareja: {}\n'.format(self.are_couple)
        msg += '\tEs FIEL: {}\n'.format(self.is_fiel)
        return msg

    def __bool__(self):
        return self.is_valid

    def _get_hash(self):
        digest = hashes.Hash(hashes.SHA512(), default_backend())
        digest.update(self._rfc.encode())
        digest.update(self._serial_number.encode())
        digest.update(TOKEN.encode())
        return digest.finalize()

    def _get_data_cer(self, cer):
        if not cer:
            return

        obj = x509.load_der_x509_certificate(cer, default_backend())
        self._rfc = obj.subject.get_attributes_for_oid(
            NameOID.X500_UNIQUE_IDENTIFIER)[0].value.split(' ')[0]
        self._serial_number = '{0:x}'.format(obj.serial_number)[1::2]
        self._not_before = obj.not_valid_before
        self._not_after = obj.not_valid_after
        now = datetime.datetime.utcnow()
        self._is_valid_time = (now > self.not_before) and (now < self.not_after)
        if not self._is_valid_time:
            msg = 'El certificado no es vigente'
            self._error = msg

        self._is_fiel = obj.extensions.get_extension_for_oid(
            ExtensionOID.KEY_USAGE).value.key_agreement

        self._cer_pem = obj.public_bytes(serialization.Encoding.PEM).decode()
        self._cer_txt = ''.join(self._cer_pem.split('\n')[1:-2])
        self._cer_modulus = obj.public_key().public_numbers().n
        return

    def _get_data_key(self, key, password):
        self._key_enc = key
        if not key or not password:
            return

        try:
            obj = serialization.load_der_private_key(
                key, password.encode(), default_backend())
        except ValueError:
            msg = 'La contraseña es incorrecta'
            self._error = msg
            return

        p = self._get_hash()
        self._key_enc = obj.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.BestAvailableEncryption(p)
        )

        self._key_modulus = obj.public_key().public_numbers().n
        self._are_couple = self._cer_modulus == self._key_modulus
        if not self._are_couple:
            msg = 'El CER y el KEY no son pareja'
            self._error = msg
        return

    def _get_key(self, password):
        if not password:
            password = self._get_hash()
        private_key = serialization.load_pem_private_key(
            self._key_enc, password=password, backend=default_backend())
        return private_key

    def _get_key_pem(self):
        obj = self._get_key('')
        key_pem = obj.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )
        return key_pem

    def sign(self, data, password=''):
        private_key = self._get_key(password)
        firma = private_key.sign(data, padding.PKCS1v15(), hashes.SHA256())
        return base64.b64encode(firma).decode()

    @property
    def rfc(self):
        return self._rfc

    @property
    def serial_number(self):
        return self._serial_number

    @property
    def not_before(self):
        return self._not_before

    @property
    def not_after(self):
        return self._not_after

    @property
    def is_fiel(self):
        return self._is_fiel

    @property
    def are_couple(self):
        return self._are_couple

    @property
    def is_valid(self):
        return not bool(self.error)

    @property
    def is_valid_time(self):
        return self._is_valid_time

    @property
    def cer_pem(self):
        return self._cer_pem.encode()

    @property
    def cer_txt(self):
        return self._cer_txt

    @property
    def key_pem(self):
        return self._get_key_pem()

    @property
    def key_enc(self):
        return self._key_enc

    @property
    def error(self):
        return self._error


class TextToDict(object):
    SAT_VERSION = '3.3'

    def __init__(self, path):
        self._error = ''
        self._data = self._load_data(path)

    def _load_data(self, path):
        ext = path.split('.')[-1].lower()
        return getattr(self, '_load_{}'.format(ext))(path)

    def _load_yaml(self, path):
        try:
            with open(path, 'r', encoding='utf-8') as f:
                data = yaml.safe_load(f)
        except yaml.parser.ParserError as e:
            self._error = str(e)
            data = {}
        return data

    def _load_json(self, path):
        try:
            if PY2:
                with codecs.open(path, 'r', encoding='utf-8') as f:
                    data = json.loads(f.read())
            else:
                with open(path, 'r', encoding='utf-8') as f:
                    data = json.loads(f.read())
        except ValueError as e:
            self._error = str(e)
            data = {}
        return data

    def _load_txt(self, path):
        self._error = 'Documento vacio'
        return {}

    def _now(self):
        return datetime.datetime.now().isoformat()[:19]

    def add_data_cert(self, cert):
        comprobante = self._data['comprobante']

        if not 'Version' in comprobante:
            comprobante['Version'] = self.SAT_VERSION
        if not 'Fecha' in comprobante:
            comprobante['Fecha'] = self._now()
        if not 'NoCertificado' in comprobante:
            comprobante['NoCertificado'] = cert.serial_number
        if not 'Certificado' in comprobante:
            comprobante['Certificado'] = cert.cer_txt

        self._data['comprobante'] = comprobante
        return

    @property
    def is_valid(self):
        return bool(self._data)

    @property
    def data(self):
        return self._data

    @property
    def error(self):
        return self._error


class SATCfdi(object):
    CURRENT = 'cfdi33'
    XSI = 'http://www.w3.org/2001/XMLSchema-instance'
    SAT = {
        'cfdi32': {
            'version': '3.2',
            'prefix': 'cfdi',
            'xmlns': 'http://www.sat.gob.mx/cfd/3',
            'schema': 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd',
        },
        'cfdi33': {
            'version': '3.3',
            'prefix': 'cfdi',
            'xmlns': 'http://www.sat.gob.mx/cfd/3',
            'schema': 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        },
        'nomina11': {
            'version': '1.1',
            'prefix': 'nomina',
            'xmlns': 'http://www.sat.gob.mx/nomina',
            'schema': 'http://www.sat.gob.mx/nomina http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina11.xsd',
        },
        'nomina12': {
            'version': '1.2',
            'prefix': 'nomina',
            'xmlns': 'http://www.sat.gob.mx/nomina12',
            'schema': 'http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd',
        },
    }

    def __init__(self, data):
        self._data = data
        self._default = self.SAT[self.CURRENT]
        self._ns = '{{{}}}'.format(self._default['xmlns'])
        self._is_valid = False
        self._error = ''
        self._xml = ''
        self._root = None

    def make_xml(self):
        self._comprobante()
        self._emisor()
        self._receptor()
        self._conceptos()
        self._impuestos()
        self._to_string()
        return

    def _comprobante(self):
        nsmap = {
            'cfdi': self._default['xmlns'],
            'xsi': self.XSI,
        }
        schema_location = ET.QName(self.XSI, 'schemaLocation')
        node_name = '{}Comprobante'.format(self._ns)
        attrib = OrderedDict(self._data['comprobante'])
        attrib[schema_location] = self._default['schema']
        self._root = ET.Element(node_name, attrib, nsmap=nsmap)
        return

    def _emisor(self):
        attrib = OrderedDict(self._data['emisor'])
        node_name = '{}Emisor'.format(self._ns)
        emisor = ET.SubElement(self._root, node_name, attrib)
        return

    def _receptor(self):
        attrib = OrderedDict(self._data['receptor'])
        node_name = '{}Receptor'.format(self._ns)
        emisor = ET.SubElement(self._root, node_name, attrib)
        return

    def _conceptos(self):
        conceptos = self._data['conceptos']
        node_name = '{}Conceptos'.format(self._ns)
        node_parent = ET.SubElement(self._root, node_name)
        for c in conceptos:
            complement = c.pop('complemento', {})
            taxes = c.pop('impuestos', {})

            node_name = '{}Concepto'.format(self._ns)
            node_child = ET.SubElement(node_parent, node_name, OrderedDict(c))

            if taxes:
                node_name = '{}Impuestos'.format(self._ns)
                node_tax = ET.SubElement(node_child, node_name)
                if taxes.get('traslados', ''):
                    node_name = '{}Traslados'.format(self._ns)
                    node = ET.SubElement(node_tax, node_name)
                    node_name = '{}Traslado'.format(self._ns)
                    for t in taxes['traslados']:
                        ET.SubElement(node, node_name, OrderedDict(t))
                if taxes.get('retenciones', ''):
                    node_name = '{}Retenciones'.format(self._ns)
                    node = ET.SubElement(node_tax, node_name)
                    node_name = '{}Retencion'.format(self._ns)
                    for t in taxes['retenciones']:
                        ET.SubElement(node, node_name, OrderedDict(t))

            # ~ if 'InformacionAduanera' in row:
                # ~ for field in fields:
                    # ~ if field in row['InformacionAduanera']:
                        # ~ attributes[field] = row['InformacionAduanera'][field]
                # ~ if attributes:
                    # ~ node_name = '{}:InformacionAduanera'.format(self._pre)
                    # ~ ET.SubElement(concepto, node_name, attributes)

            # ~ if 'CuentaPredial' in row:
                # ~ attributes = {'numero': row['CuentaPredial']}
                # ~ node_name = '{}:CuentaPredial'.format(self._pre)
                # ~ ET.SubElement(concepto, node_name, attributes)

            # ~ if 'autRVOE' in row:
                # ~ fields = (
                    # ~ 'version',
                    # ~ 'nombreAlumno',
                    # ~ 'CURP',
                    # ~ 'nivelEducativo',
                    # ~ 'autRVOE',
                # ~ )
                # ~ for field in fields:
                    # ~ if field in row['autRVOE']:
                        # ~ attributes[field] = row['autRVOE'][field]
                # ~ node_name = '{}:ComplementoConcepto'.format(self._pre)
                # ~ complemento = ET.SubElement(concepto, node_name)
                # ~ ET.SubElement(complemento, 'iedu:instEducativas', attributes)

        return

    def _impuestos(self):
        node_name = '{}Impuestos'.format(self._ns)
        taxes = self._data.get('impuestos', False)
        if not taxes:
            ET.SubElement(self._root, node_name)
            return

        traslados = taxes.pop('traslados', False)
        retenciones = taxes.pop('retenciones', False)
        node = ET.SubElement(self._root, node_name, OrderedDict(taxes))

        if traslados:
            node_name = '{}Traslados'.format(self._ns)
            sub_node = ET.SubElement(node, node_name)
            node_name = '{}Traslado'.format(self._ns)
            for t in traslados:
                ET.SubElement(sub_node, node_name, OrderedDict(t))

        if retenciones:
            node_name = '{}Retenciones'.format(self._ns)
            sub_node = ET.SubElement(node, node_name)
            node_name = '{}Retencion'.format(self._ns)
            for r in retenciones:
                ET.SubElement(sub_node, node_name, OrderedDict(r))
        return

    def _to_string(self):
        self._xml = ET.tostring(self._root,
            pretty_print=True, xml_declaration=True, encoding='utf-8')
        self._is_valid = True
        return

    @property
    def is_valid(self):
        return self._is_valid

    @property
    def xml(self):
        return self._xml.decode('utf-8')

    @property
    def error(self):
        return self._error


class CfdiStamp(object):
    REGIMEN = '601'

    def __init__(self, obj, xslt, cert):
        self._xml = ''
        self._doc = None
        self._root = None
        self._is_valid = False
        self._parse(obj)
        self._add_data_cert(cert)
        self._cadena = self._get_cadena(xslt)
        self._add_stamp(cert)

    def _parse(self, obj):
        self._doc = ET.parse(obj)
        self._root = self._doc.getroot()
        return

    def _now(self):
        return datetime.datetime.now().isoformat()[:19]

    def _add_data_cert(self, cert):
        if DEBUG:
            ns = {'cfdi': 'http://www.sat.gob.mx/cfd/3'}
            self._root.attrib['Fecha'] = self._now()
            self._root.attrib['NoCertificado'] = cert.serial_number
            self._root.attrib['Certificado'] = cert.cer_txt
            emisor = self._root.xpath('//cfdi:Emisor', namespaces=ns)[0]
            emisor.attrib['Rfc'] = cert.rfc
            emisor.attrib['RegimenFiscal'] = self.REGIMEN
        return

    def _get_cadena(self, xslt):
        transfor = ET.XSLT(ET.parse(xslt))
        if PY2:
            cadena = str(transfor(self._doc))
        else:
            cadena = str(transfor(self._doc)).encode()
        return cadena

    def _add_stamp(self, cert):
        stamp = cert.sign(self._cadena)
        self._root.attrib['Sello'] = stamp
        self._xml = ET.tostring(self._root, pretty_print=True,
            xml_declaration=True, encoding='utf-8')
        self._is_valid = True
        return

    @property
    def is_valid(self):
        return self._is_valid

    @property
    def xml(self):
        return self._xml.decode('utf-8')


class SATPac(object):

    def __init__(self):
        self.error = ''
        self._pac = self._get_pac()

    def _get_pac(self):
        from finkok import PACFinkok

        pac = {
            'finkok': PACFinkok
        }
        return pac[PAC]()

    def cfdi_stamp(self, xml):
        result = self._pac.cfdi_stamp(xml)
        if result:
            return result['xml']

        self.error = self._pac.error
        return {}

    def cfdi_cancel(self, rfc, uuid, cer, key):
        result = self._pac.cfdi_cancel(rfc, uuid, cer, key)
        if result:
            return result

        self.error = self._pac.error
        return {}

    def cfdi_status(self, uuid):
        result = self._pac.cfdi_status(uuid)
        if result:
            return result

        self.error = self._pac.error
        return {}


def validate_cert(path_certificates):
    if not path_certificates:
        msg = 'Ruta de certificados (-dc), argumento requerido'
        _error(msg)
        return

    password = getpass.getpass('\nIntroduce la contraseña del certificado: ')
    if not password:
        msg = 'La contraseña es requerida'
        _error(msg)
        return

    msg = '\nValidando certificados...\n'
    _msg(msg, fg='green')

    path_cer = join(path_certificates, CERT_NAME.format('cer'))
    path_key = join(path_certificates, CERT_NAME.format('key'))
    path_enc = join(path_certificates, CERT_NAME.format('enc'))

    cer = _read_file(path_cer)
    key = _read_file(path_key)

    cert = SATCertificate(cer, key, password)
    click.secho(str(cert), bold=True)
    if cert.is_valid:
        with open(path_enc, 'wb') as f:
            f.write(cert.key_enc)
        msg = 'Los certificados son válidos\n'
        _msg(msg, fg='green')
    else:
        _error(cert.error)

    return


def generar_cfdi(path_source, path_target, path_certificates):
    files = _get_files(path_source)

    if not files:
        msg = 'No se encontraron archivos nuevos para generar CFDIs'
        _error(msg)
        return

    path_cer = ''
    if path_certificates:
        path_cer = join(path_certificates, CERT_NAME.format('cer'))

    i = 0
    t = len(files)
    for i, f in enumerate(files):
        msg = 'Generando el documento {} de {}: {}'.format(i+1, t, f)
        _msg(msg)

        obj = TextToDict(f)
        if not obj.is_valid:
            msg = '\t{}'.format(obj.error)
            _error(msg)
            continue

        if path_cer:
            cert = SATCertificate(_read_file(path_cer))
            obj.add_data_cert(cert)

        cfdi = SATCfdi(obj.data)
        cfdi.make_xml()
        if not cfdi.is_valid:
            msg = '\t{}'.format(cfdi.error)
            _error(msg)
            continue

        path_xml = join(path_target, _change_ext(f))
        _save_file(path_xml, cfdi.xml)
        _delete_source(f)

        msg = '\tDocumento generado: {}'.format(path_xml)
        _msg(msg, fg='green')

    return


def sellar_cfdi(path_source, path_certificates):
    path_cer = join(path_certificates, CERT_NAME.format('cer'))
    path_enc = join(path_certificates, CERT_NAME.format('enc'))
    files = _get_files(path_source, 'xml')

    if not files:
        msg = 'No se encontraron CFDIs nuevos para sellar'
        _error(msg)
        return

    if not _exists(path_enc):
        msg = 'No se encontró el certificado para sellar. Usa -vc para verificarlo'
        _error(msg)
        sys.exit()
        return

    i = 0
    t = len(files)
    for i, f in enumerate(files):
        msg = 'Sellando el documento {} de {}: {}'.format(i+1, t, f)
        _msg(msg)

        cer = _read_file(path_cer)
        enc = _read_file(path_enc)

        cert = SATCertificate(cer, enc)
        fo = open(f, 'rb')
        xslt = open(PATH_XSLT, 'rb')

        stamp = CfdiStamp(fo, xslt, cert)
        if stamp.is_valid:
            _save_file(f, stamp.xml)
            msg = '\tDocumento sellado...'
            _msg(msg, fg='green')

    return


def timbrar_cfdi(path_source, path_target):
    files = _get_files(path_source, 'xml')

    if not files:
        msg = 'No se encontraron CFDIs nuevos para sellar'
        _error(msg)
        return

    i = 0
    t = len(files)
    for i, f in enumerate(files):
        msg = 'Enviando a timbrar el documento {} de {}: {}'.format(i+1, t, f)
        _msg(msg)
        pac = SATPac()
        xml = pac.cfdi_stamp(_read_file(f))
        if pac.error:
            _error('\t' + pac.error)
            continue

        path_xml = join(path_target, _get_file_name(f))
        _save_file(path_xml, xml)
        _delete_source(f)
        msg = '\tDocumento timbrado: {}'.format(path_xml)
        _msg(msg, fg='green')

    return


def cancelar_cfdi(uuid, path_certificates, path_source=''):
    path_cer = join(path_certificates, CERT_NAME.format('cer'))
    path_enc = join(path_certificates, CERT_NAME.format('enc'))

    cer = _read_file(path_cer)
    enc = _read_file(path_enc)
    cert = SATCertificate(cer, enc)

    pac = SATPac()
    if uuid:
        result = pac.cfdi_cancel(cert.rfc, uuid, cert.cer_pem, cert.key_pem)
        if result:
            print(result)
            msg = 'Documento cancelado'
            _msg(msg, fg='green')
        else:
            _error('\t' + pac.error)
    return


def estatus_cfdi(uuid, path_source):
    pac = SATPac()
    if uuid:
        result = pac.cfdi_status(uuid)
        if result:
            msg = '\tEstatus: {}\n\tFecha: {}'.format(
                result['estatus'], result['fecha'])
            _msg(msg, fg='green')
        else:
            _error('\t' + pac.error)
    return


def validar_dir(source, target, stamp):
    msg = 'No se encontró el directorio '
    if not _exists(source):
        _error('\t' + msg + 'de entrada: ' + source)
        return False
    if not _exists(target):
        _error('\t' + msg + 'de salida: ' + target)
        return False
    if not _exists(stamp):
        _error('\t' + msg + 'para timbrados ' + stamp)
        return False

    return True
