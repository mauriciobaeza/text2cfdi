<xsl:stylesheet version = '1.0'
    xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
    xmlns:cfdi="http://www.sat.gob.mx/cfd/3">

<xsl:output method = "text" />

<xsl:template match="/">-----BEGIN CERTIFICATE-----<xsl:text>&#10;</xsl:text><xsl:apply-templates select="/cfdi:Comprobante"/>-----END CERTIFICATE-----</xsl:template>

<xsl:template match="cfdi:Comprobante">
    <xsl:call-template name="get-line">
        <xsl:with-param name="text" select="@Certificado"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="get-line">
    <xsl:param name="text"/>
    <xsl:param name="max-length" select="64"/>
    <xsl:variable name="line">
        <xsl:value-of select="substring($text, 1, $max-length)"/>
    </xsl:variable>
    <xsl:variable name="line2">
        <xsl:value-of select="substring($text, $max-length+1)"/>
    </xsl:variable>

    <xsl:value-of select="$line"/><xsl:text>&#10;</xsl:text>

    <xsl:choose>
        <xsl:when test="string-length($line2) &gt; $max-length">
            <xsl:call-template name="get-line">
                <xsl:with-param name="text" select="$line2"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$line2"/><xsl:text>&#10;</xsl:text>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

</xsl:stylesheet>
